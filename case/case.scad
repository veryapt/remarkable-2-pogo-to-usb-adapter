// Tablet height: 246mm
// Tablet thickness: 4.8mm
// Grey border on left: 13.5mm
// From grey to screen: 9mm
// From edge to first pogo pad: 13.5mm

include <fillet.scad>;
include <roundedCube.scad>;

wall=2;
slop = 0.1;
tablet = [13.5 + 9, 246.5 + slop, 4.8 + slop];
pcb = [10.5, 42, 1.75];
curve_height = 2.5;

module edge_cavity(size) {
    translate([size.x/2, 0, size.z/2])
        resize(size)
            rotate([-90, 0, 0])
                cylinder(h=size.y, d=size.z, $fn=100);
}

module tablet_cavity(size) {
    translate([curve_height/2, curve_height, 0])
        rotate([0, 0, -90])
            edge_cavity([curve_height, size.x - curve_height/2, size.z]);

    translate([curve_height/2, size.y, 0])
        rotate([0, 0, -90])
            edge_cavity([curve_height, size.x - curve_height/2, size.z]);

    translate([0, curve_height/2, 0])
        edge_cavity([curve_height, size.y-curve_height, size.z]);
    
    // corner 1
    translate([curve_height/2, curve_height/2, size.z/2])
        resize([curve_height, curve_height, size.z], $fn=100)
            sphere(r=curve_height);
    
    // corner 2
    translate([curve_height/2, size.y-curve_height/2, size.z/2])
        resize([curve_height, curve_height, size.z], $fn=100)
            sphere(r=curve_height);
    
    // fill in the space between the edges
    translate([curve_height/2, curve_height/2, 0])
        cube([size.x - curve_height/2, size.y - curve_height, size.z]);
}

module pogo_pins() {
    // pogo pin housing
    translate([-pcb.x, (2.54 * 2) - 13.5/2, -2.75/2])
        cube([pcb.x + 3.5, 13.5, 2.75]);
    
    /* Holes for pins result in pins getting stuck
    pin_len = 3;
    // holes for pogo pins
    rotate([0, 90, 0])
        cylinder(h=pin_len, d=2, $fn=100);
            
    translate([0, 2.54, 0])
        rotate([0, 90, 0])
            cylinder(h=pin_len, d=2, $fn=100);
            
    translate([0, 2.54*2, 0])
        rotate([0, 90, 0])
            cylinder(h=pin_len, d=2, $fn=100);
            
    translate([0, 2.54*3, 0])
        rotate([0, 90, 0])
            cylinder(h=pin_len, d=2, $fn=100);
    
    translate([0, 2.54*4, 0])
        rotate([0, 90, 0])
            cylinder(h=pin_len, d=2, $fn=100);
    */
}

pcb_bottom = -pcb.z - 2.6/2;

module pcb_cavity() {
    translate([-slop, 0, pcb_bottom-slop/2])
        #cube([pcb.x, pcb.y + slop*2, pcb.z+slop]);
    
    translate([pcb.x, 2.5, 0])
        pogo_pins();
    
    // pogo pin legs
    translate([0, 1, pcb_bottom - 2.5])
        cube([3.5, 13, 2.5]);
    
    // usb breakout
    translate([1, pcb.y - 21.5, pcb_bottom - 5 - 2.5])
        #cube([15.5, 21.5, 5]);
    
    // header pins
    translate([1, pcb.y - 21.5, pcb_bottom - 2.5])
        #cube([pcb.x - 1, 21.5, 2.5 + pcb.z + 1]);
    
    // usb breakout board offset: 1mm
    // usb breakout board width: 20.4mm
    translate([15.5, pcb.y - 9.5/2 - 0.5 - 19.4/2, pcb_bottom  - 2.75 - 5 + 3.75])
    rotate([0, 90, 0])
    hull() {
        translate([3.75/2, 3.75/2, 0])
            cylinder(h=wall, d=3.75, $fn=100);
        translate([3.75/2, 10 - 3.75/2, 0])
            cylinder(h=wall, d=3.75, $fn=100);
    }
}

module cutaway() {
    difference() {
        size = [15.5 + wall, 10 + wall, tablet.z + wall*2];
        resize([size.x*2, size.y*2, size.z])
            cylinder(r=size.x, h=size.z);
        translate([-pcb.x, -10 - wall, 0])
        cube([pcb.x + wall*2 + 0.5, 20 + wall*2, size.z]);
    }
}

module tablet_shell() {
    difference() {
        fillet(r=wall,steps=50) {
        //union() {
            translate([-pcb.x - wall - 0.5, 8, pcb_bottom  - 2.75 - 5 + 3.75 - 0.6 - wall])
            roundedCube([15 + wall*2, pcb.y + wall*4, 5 + 0.6 + wall*2], r=wall, $fn=50);
            translate([-wall*2 - pcb.x, -wall, -wall])
                    tablet_cavity([tablet.x + pcb.x + wall*2, tablet.y + wall*2, tablet.z + wall*2]);
        }
        
        // channel for power stud
        translate([wall, tablet.y-1, 1])
            #cube([tablet.x, 2, 3]);
        
        // cutouts for nubs on back
        translate([tablet.x, 11, -wall])
            #cylinder(h=wall, d=6, $fn=50);
        
        translate([tablet.x, tablet.y - 11, -wall])
            #cylinder(h=wall, d=6, $fn=50);
        
        
        tablet_cavity(tablet);
        
        translate([-wall/2 - pcb.x, 11.5, tablet.z/2])
            pcb_cavity();
        
        translate([-wall*2, -wall, -wall])
            cutaway();
        
        translate([-wall*2, tablet.y + wall, -wall])
            cutaway();
                
    }
}

module pcb_cavity_test() {
    difference() {
        block = [17+wall, pcb.y +wall*2, 2.6 + pcb.z + 2.5 + 5 +wall*2];
        translate([0, -wall, -block.z + 2.6/2 +wall])
        cube(block);
        pcb_cavity();
        
        translate([pcb.x + wall/2, -wall, -block.z + 2.6/2 +wall])
            cube([17 - pcb.x + wall/2, 13 + wall + 1, block.z]);
    }
}

module screw_test() {
    difference() {
        cube([7, 6, 6]);
        translate([0, 3, 3])
        //screw(true);
        screw(false);
    }
}

module screw(length, loose = false) {
    size = loose ? 2.25 : 2;
    length = length + 0.25;
    
    rotate([0, 90, 0])
    union() {
        hull() {
            // slop = 0.1
            translate([0, 0, -4])
                cylinder(d=4.25, h=4, $fn=50);
            translate([0, 0, 1.34])
                cylinder(d=2.25, h=0.01, $fn=50);
        }
        cylinder(h=length, d=size, $fn=100);
    }
}

module screws(loosen=false) {
    size = loosen ? 2.25 : 2.1;
    
    translate([-14, 4, tablet.z/2])
        screw(8);
        
    translate([-12.5, 11, -6.5])
        screw(8);
    
    translate([-12.5, 55.5, -6.5])
        screw(8);
    
    translate([-14, 55.5, 4])
        screw(8);
}

module magnet() {
    size = [1.66, 4];
    translate([-1.66, 0, 0])
    rotate([0, 90, 0])
        cylinder(h=size[0], d=size[1] + 0.25, $fn=100);
}

module magnets() {
    positions = [tablet.y - 15, tablet.y - 31];
    for (position = positions) {
        translate([0, position, tablet.z/2])
            magnet();
    }
}

module top_part() {
    difference() {
        intersection() {
            tablet_shell();
            translate([-15, -wall, -10])
                cube([6, pcb.y + wall*2 + 15 + 0.75, 20]);
        }
        #screws(true);
        //#magnets();
    }
}

module bottom_part() {
    difference() {
        //intersection() {
            tablet_shell();
            translate([-15, -wall, -10])
                cube([6, pcb.y + wall*2 + 15 + 0.75, 20]);
        //}
        screws(false);
        translate([-2, 0, 0])
            #magnets();
    }
}


rotate([0,-90, 0])
    bottom_part();


//rotate([0, 90, 0])
//    top_part();