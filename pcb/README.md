# pcb

This PCB is very simple, consisting only of traces running from the Adafruit
USB-C breakout board to the pogo pins. It could be replaced entirely with wires,
but I find the mechanical properties of a PCB convenient in this context. It
should be possible to come up with a variant of this design that only uses
wires, but PCBs are cheap enough and simplify enough for me that I found this
approach worthwhile.

You should be able to just zip up the gerbers directory and submit the zip file
to a PCB manufacturing service, such as [OSHPark](https://oshpark.com/) or
[7pcb](https://www.7pcb.com/).

The Adafruit breakout and the pogo pins should be soldered to opposite sides of
the board, both facing in the same direction. Neither the name of the board nor
the "open source hardware gear" graphic should be covered if you have them on
the correct sides.

The Adafruit breakout board should have the provided header pins soldered
between it and the PCB in order to create enough space between the breakout
board and the pogo pins to clear the tablet.

Print the case and examine the cavity provided for the PCB if the above
description seems unclear to you.

This is a KiCad 6.0 project, so you'll need to install KiCad if you want to make
modifications.
